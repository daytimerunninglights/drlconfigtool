#include <QtWidgets/QApplication>
#include <QTranslator>
#include "mainwindow.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QTranslator appTranslator;
    QCoreApplication::setOrganizationName(QLatin1String("SignalNet"));
    QCoreApplication::setApplicationName(QLatin1String("DRLConfigTool"));

    appTranslator.load(QLatin1String("DRLConfigTool_")+QLocale::system().name(),
                       a.applicationDirPath());

    a.installTranslator(&appTranslator);
    MainWindow w;
    w.show();
    
    return a.exec();
}
