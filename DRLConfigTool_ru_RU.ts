<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>About</name>
    <message>
        <location filename="about.ui" line="29"/>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="about.ui" line="50"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;img src=&quot;:/Images/CompanyLogo&quot; /&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;img src=&quot;:/Images/ProgramLogo&quot; /&gt;&lt;span style=&quot; font-size:8.25pt;&quot;&gt; &lt;/span&gt;&lt;span style=&quot; font-size:18pt; font-weight:600;&quot;&gt;DRLConfigTool&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt;Version 1.0&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt;&quot;&gt;Developer: Tumanov Stanislav Aleksandrovich&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt;&quot;&gt;Kirov, Russia 2014&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt;&quot;&gt;email: &lt;/span&gt;&lt;a href=&quot;mailto:maintumanov@mail.ru&quot;&gt;&lt;span style=&quot; font-size:12pt; text-decoration: underline; color:#0000ff;&quot;&gt;maintumanov@mail.ru&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;img src=&quot;:/Images/CompanyLogo&quot; /&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;img src=&quot;:/Images/ProgramLogo&quot; /&gt;&lt;span style=&quot; font-size:8.25pt;&quot;&gt; &lt;/span&gt;&lt;span style=&quot; font-size:18pt; font-weight:600;&quot;&gt;DRLConfigTool&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt;Версия 1.0&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt;&quot;&gt;Автор: Туманов Станислав Александрович&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt;&quot;&gt;Киров, Россия 2014&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt;&quot;&gt;email: &lt;/span&gt;&lt;a href=&quot;mailto:maintumanov@mail.ru&quot;&gt;&lt;span style=&quot; font-size:12pt; text-decoration: underline; color:#0000ff;&quot;&gt;maintumanov@mail.ru&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="about.ui" line="94"/>
        <source>Close</source>
        <translation>Выйти</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>TextLabel</source>
        <translation type="vanished">Метка</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="79"/>
        <source>Select COM port setting</source>
        <translation>Настройка подключения</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="108"/>
        <source>Port:</source>
        <translation>Порт:</translation>
    </message>
    <message>
        <source>Speed:</source>
        <translation type="vanished">Скорость:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="131"/>
        <source>Connect</source>
        <translation>Подключиться</translation>
    </message>
    <message>
        <source>Load setting</source>
        <translation type="vanished">Загрузка настроек</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation type="vanished">Предупреждение</translation>
    </message>
    <message>
        <source>Failed to load the settings, check the port settings and connection speed!</source>
        <translation type="vanished">Не удалось подключиться, пожалуйста проверьте еще раз параметры настроек соединения и попробуйте еще раз!</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="vanished">Выход</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="150"/>
        <source>Configuring the positive inputs</source>
        <translation>Конфигурация положительных входов</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>DRL ConfigTool</source>
        <translation>Конфигуратор ДХО</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="158"/>
        <source>Input 1</source>
        <translation>Вход 1</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="200"/>
        <source>Input 2</source>
        <translation>Вход 2</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="242"/>
        <source>Input 3</source>
        <translation>Вход 3</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="285"/>
        <source>Configuring the negative inputs</source>
        <translation>Конфигурация негативных входов</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="293"/>
        <source>~Input 4</source>
        <translation>~Вход 4</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="335"/>
        <source>~Input 5</source>
        <translation>~Вход 5</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="378"/>
        <source>Setting the voltage sensor</source>
        <translation>Настройки датчика напряжения бортсети</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="401"/>
        <source>Switch-on threshold, V</source>
        <translation>Порог включения, Вольт</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="437"/>
        <source>Switch-off threshold, V</source>
        <translation>Порог выключения, Вольт</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="634"/>
        <source>Delay On</source>
        <translation>Задержка включения</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="648"/>
        <source>Delay, ms</source>
        <translation>Задержка, мс</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="779"/>
        <source>Disconnect</source>
        <translation>Отключиться</translation>
    </message>
    <message>
        <source>Message</source>
        <translation type="obsolete">Сообщение</translation>
    </message>
    <message>
        <source>Tthreshold (on), V</source>
        <translation type="vanished">Порог включения</translation>
    </message>
    <message>
        <source>Tthreshold (off), V</source>
        <translation type="vanished">Порог выключения</translation>
    </message>
    <message>
        <source>Current voltage in the wiring: 0 volts.</source>
        <translation type="vanished">Текущее напряжение в борт сети: 0 вольт.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="510"/>
        <source>Adjusting the brightness</source>
        <translation>Настройка яркости</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="524"/>
        <source>The brightness of the lights. %</source>
        <translation>Яркость ходовых огней, %</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="563"/>
        <source>Coefficient smooth inclusion</source>
        <translation>Коофициент плавности включения огней</translation>
    </message>
    <message>
        <source>Curent brightness: 0 %</source>
        <translation type="obsolete">Текущая яркость: %</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="718"/>
        <source>Save and exit</source>
        <translation>Сохранение настроек и выход</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="765"/>
        <source>Save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">Выйти</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="73"/>
        <source>Unable to connect!</source>
        <translation>Не удалось подключиться!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="79"/>
        <source>Current voltage in the wiring: %1 volts.</source>
        <translation>Текущее напряжение в бортсети: %1 вольт.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="102"/>
        <source>Curent brightness: %1 %</source>
        <translation>Текущая яркость: %1 %</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="227"/>
        <source>Settings saved successfully!</source>
        <translation>Настройки успешно сохранены!</translation>
    </message>
</context>
<context>
    <name>QRS232</name>
    <message>
        <location filename="qrs232.cpp" line="28"/>
        <location filename="qrs232.cpp" line="72"/>
        <source>Unable to open comport</source>
        <translation>Ошибка открытия порта</translation>
    </message>
    <message>
        <location filename="qrs232.cpp" line="37"/>
        <source>unable to read port settings</source>
        <oldsource>unable to read portsettings</oldsource>
        <translation>Ошибка чтения настроек порта</translation>
    </message>
    <message>
        <location filename="qrs232.cpp" line="62"/>
        <source>unable to adjust port settings </source>
        <oldsource>unable to adjust portsettings </oldsource>
        <translation>Ошибка настройки порта</translation>
    </message>
    <message>
        <location filename="qrs232.cpp" line="82"/>
        <source>Unable to set comport dcb settings</source>
        <translation>Ошибка настройки вывода DCB</translation>
    </message>
    <message>
        <location filename="qrs232.cpp" line="89"/>
        <source>Unable to set comport cfg settings</source>
        <translation>Ошибка настройки вывода CFG</translation>
    </message>
    <message>
        <location filename="qrs232.cpp" line="104"/>
        <source>Unable to set comport timeout settings</source>
        <oldsource>Unable to set comport time-out settings</oldsource>
        <translation>Время ожидания настройки порта, вышло</translation>
    </message>
</context>
</TS>
