#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFrame>
#include <QCheckBox>
#include "qpl.h"
#include "about.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT


public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    QPL *PL;
    
private slots:

    void on_button_close_clicked();
    void on_dspin_voltage_on_valueChanged(double arg1);
    void on_dspin_voltage_off_valueChanged(double arg1);
    void on_spin_pwm_valueChanged(int arg1);
    void on_spin_smooth_valueChanged(int arg1);
    void on_box_voltage_clicked();
    void on_button_save_clicked();
    void on_check_input1_clicked();
    void on_check_input2_clicked();
    void on_check_input3_clicked();
    void on_check_input4_clicked();
    void on_check_input5_clicked();

    void StateConnectChange(bool state);
    void ReciveData(quint8 Channel, QVector<quint8> Data);
    void set_mode_gui(int mode);
    void start_load_settings();
    void voltage_view(quint16 volume);
    void voltage_level_set_hi(quint16 level);
    void voltage_level_set_lo(quint16 level);
    void pwm_view(quint16 volume);
    void pwm_level_set(quint16 level);
    void delay_view(quint8 volume);
    void input_view(quint8 inp);
    void input_view_indicate(QCheckBox *chBox, QFrame *frm, bool act);
    void error_connect();
    void showMessage(int importance, QString msg);
    void showMessageTimeOut();
    void setFrameColor(int importance, QFrame *frame);
    void loadSettings();
    void saveSettings();
    void on_button_info_clicked();
    void on_spin_delay_valueChanged(int arg1);

private:
    double vConst;
    Ui::MainWindow *ui;
    quint8 programState;
    QTimer *msgTimer;


};

#endif // MAINWINDOW_H
