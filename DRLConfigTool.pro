#-------------------------------------------------
#
# Project created by QtCreator 2012-09-20T21:21:01
#
#-------------------------------------------------

QT       += core widgets

TARGET = DRLConfigTool
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    qrs232.cpp \
    qpl.cpp \
    about.cpp

HEADERS  += mainwindow.h \
    qrs232.h \
    qpl.h \
    about.h

FORMS    += mainwindow.ui \
    about.ui



TRANSLATIONS = DRLConfigTool_ru_RU.ts

win32 {
        RC_FILE += applico.rc
        OTHER_FILES += applico.rc
}

RESOURCES += \
    resource.qrc
