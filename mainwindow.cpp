#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    vConst = 52.50;
    set_mode_gui(0);
    msgTimer = new QTimer(this);
    PL = new QPL(this);
    PL->SetEnabled(true);
    ui->combo_port->addItems(*PL->GetPortList());

    connect(PL,SIGNAL(StateChange(bool)),this,SLOT(StateConnectChange(bool)));
    connect(ui->combo_port,SIGNAL(currentIndexChanged(int)),PL, SLOT(SetCOMPort(int)));
    connect(PL,SIGNAL(ReciveData(quint8,QVector<quint8>)),this,SLOT(ReciveData(quint8,QVector<quint8>)));

    PL->SetCOMPort(0);
    PL->SetSpeed(4);
    ui->combo_port->setCurrentIndex(0);
    ui->frame_msg->setVisible(false);
    connect(msgTimer, SIGNAL(timeout()), this, SLOT(showMessageTimeOut()));
    connect(ui->button_msg, SIGNAL(clicked()), this, SLOT(showMessageTimeOut()));
    connect(ui->button_connect, SIGNAL(clicked()), PL, SLOT(OpenPort()));
    loadSettings();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_button_close_clicked()
{
    PL->SendInt8(15,1);
    PL->ClosePort();
}

void MainWindow::StateConnectChange(bool state)
{
    if (state) start_load_settings();
    else set_mode_gui(0);
}

void MainWindow::set_mode_gui(int mode)
{
    programState = mode;
    ui->box_connect->setVisible(mode < 2);
    ui->button_connect->setEnabled(mode != 1);
    ui->box_negativeInput->setVisible(mode > 1);
    ui->box_positiveInput->setVisible(mode > 1);
    ui->box_pwm->setVisible(mode == 2);
    ui->box_delay->setVisible(mode == 3);
    ui->box_voltage->setVisible(mode > 1);
    ui->box_save->setVisible(mode > 1);
}

void MainWindow::start_load_settings()
{
    set_mode_gui(1);
    QTimer::singleShot(3000, this, SLOT(error_connect()));
    PL->SendInt16(0,1);
}

void MainWindow::error_connect()
{
    if (programState != 1) return;
    PL->ClosePort();
    showMessage(0, QString(tr("Unable to connect!")));
}

void MainWindow::voltage_view(quint16 volume)
{
    double vol = (double)volume / vConst;
    ui->output_voltage->setText(QString(tr("Current voltage in the wiring: %1 volts.")).arg(vol,0, 'f', 1));
    int importance = 0;
    if (vol > ui->dspin_voltage_off->value()) importance = 1;
    if (vol > ui->dspin_voltage_on->value()) importance = 2;
    if (!ui->box_voltage->isChecked()) importance = 3;
    setFrameColor(importance, ui->frame_voltage_indicator);
}

void MainWindow::voltage_level_set_hi(quint16 level)
{
    double vol = (double)level / vConst;
    ui->dspin_voltage_on->setValue(vol);
}

void MainWindow::voltage_level_set_lo(quint16 level)
{
    double vol = (double)level / vConst;
    ui->dspin_voltage_off->setValue(vol);
}

void MainWindow::pwm_view(quint16 volume)
{
    quint32 vol = (100  * (quint32)volume) / 1000;
    ui->output_pwm->setText(QString(tr("Curent brightness: %1 %")).arg(vol));
    int importance = 0;
    if (vol > 0) importance = 1;
    if ((int)vol == ui->spin_pwm->value()) importance = 2;
    setFrameColor(importance, ui->frame_pwm_indicator);
}

void MainWindow::pwm_level_set(quint16 level)
{
    quint32 vol = (100  * (quint32)level) / 1000;
    ui->spin_pwm->setValue((quint16)vol);
}

void MainWindow::delay_view(quint8 volume)
{
    int importance = 0;
    if (volume == 1) importance = 2;
    setFrameColor(importance, ui->frame_lights_indicator);
}


void MainWindow::input_view(quint8 inp)
{
    input_view_indicate(ui->check_input1, ui->frame_input1_indicator, (inp & (quint8)1));
    input_view_indicate(ui->check_input2, ui->frame_input2_indicator, (inp & (quint8)2));
    input_view_indicate(ui->check_input3, ui->frame_input3_indicator, (inp & (quint8)4));
    input_view_indicate(ui->check_input4, ui->frame_input4_indicator, !  (inp & (quint8)8));
    input_view_indicate(ui->check_input5, ui->frame_input5_indicator, (inp & (quint8)16));
}

void MainWindow::input_view_indicate(QCheckBox *chBox, QFrame *frm, bool act)
{
    if (chBox->isChecked() && act) setFrameColor(0, frm);
    if (chBox->isChecked() && !act) setFrameColor(2, frm);
    if (!chBox->isChecked() && act) setFrameColor(1, frm);
    if (!chBox->isChecked() && !act) setFrameColor(3, frm);
}

void MainWindow::showMessage(int importance, QString msg)
{
    setFrameColor(importance, ui->frame_msg);
    ui->label_msg->setText(msg);
    ui->frame_msg->setVisible(true);
    msgTimer->start(8000);
}

void MainWindow::showMessageTimeOut()
{
    ui->frame_msg->setVisible(false);
    msgTimer->stop();
}

void MainWindow::setFrameColor(int importance, QFrame *frame)
{
    QPalette pal = frame->palette();
    switch (importance)
    {
    case 0:
        pal.setBrush(QPalette::Background, QBrush(QColor(254, 138, 145, 255), Qt::SolidPattern));
        break;
    case 1:
        pal.setBrush(QPalette::Background, QBrush(QColor(254, 241, 138, 255), Qt::SolidPattern));
        break;
    case 2:
        pal.setBrush(QPalette::Background, QBrush(QColor(172, 246, 176, 255), Qt::SolidPattern));
        break;
    case 3:
        pal = ui->box_pwm->palette();
        break;
    }
    frame->setPalette(pal);
}

void MainWindow::loadSettings()
{
    QSettings sttings;
    ui->combo_port->setCurrentIndex(sttings.value("last_connect_num_port",0).toInt());
}

void MainWindow::saveSettings()
{
    QSettings sttings;
    sttings.setValue("last_connect_num_port", ui->combo_port->currentIndex());
}

void MainWindow::ReciveData(quint8 Channel, QVector<quint8> Data)
{
    switch (Channel)
    {
    case 0:
        if (PL->GetInt8FromData(&Data) == 1) set_mode_gui(2);
        if (PL->GetInt8FromData(&Data) == 2) set_mode_gui(3);
        saveSettings();
        break;
    case 1:
        ui->check_input1->setChecked(PL->GetInt8FromData(&Data));
        break;
    case 2:
        ui->check_input2->setChecked(PL->GetInt8FromData(&Data));
        break;
    case 3:
        ui->check_input3->setChecked(PL->GetInt8FromData(&Data));
        break;
    case 4:
        ui->check_input4->setChecked(PL->GetInt8FromData(&Data));
        break;
    case 5:
        ui->check_input5->setChecked(PL->GetInt8FromData(&Data));
        break;
    case 6:
        ui->box_voltage->setChecked((PL->GetInt8FromData(&Data) == 1));
        break;
    case 7:
        voltage_level_set_hi(PL->GetInt16FromData(&Data));
        break;
    case 8:
        voltage_level_set_lo(PL->GetInt16FromData(&Data));
        break;
    case 9:
        pwm_level_set(PL->GetInt16FromData(&Data));
        break;
    case 10:
        ui->spin_smooth->setValue(PL->GetInt16FromData(&Data) / 50);
        break;
    case 11:
        showMessage(2, QString(tr("Settings saved successfully!")));
        break;
    case 12:
        voltage_view(PL->GetInt16FromData(&Data));
        break;
    case 13:
        pwm_view(PL->GetInt16FromData(&Data));
        break;
    case 14:
        input_view(PL->GetInt8FromData(&Data));
        break;
    case 16:
        ui->spin_delay->setValue(PL->GetInt16FromData(&Data));
        ui->output_delay_progress->setMaximum(ui->spin_delay->value());
        break;
    case 17:
        ui->output_delay_progress->setValue(PL->GetInt16FromData(&Data));
        break;
    case 18:
        delay_view(PL->GetInt8FromData(&Data));
        break;
    case 20:
        qDebug() << "debug read " << PL->GetInt16FromData(&Data);
        break;
    }
}

void MainWindow::on_spin_pwm_valueChanged(int arg1)
{
    quint32 vol = 10 * arg1;
    PL->SendInt16(9,(quint16)vol);
}

void MainWindow::on_box_voltage_clicked()
{
    PL->SendInt8(6,(quint8)ui->box_voltage->isChecked());
}

void MainWindow::on_dspin_voltage_on_valueChanged(double arg1)
{
    double resvol = arg1 * vConst;
    PL->SendInt16(7,(quint16)resvol);
    if (ui->dspin_voltage_off->value() > ui->dspin_voltage_on->value())
        ui->dspin_voltage_off->setValue(ui->dspin_voltage_on->value());
}

void MainWindow::on_dspin_voltage_off_valueChanged(double arg1)
{
    double resvol = arg1 * vConst;
    PL->SendInt16(8,(quint16)resvol);
    if (ui->dspin_voltage_off->value() > ui->dspin_voltage_on->value())
        ui->dspin_voltage_on->setValue(ui->dspin_voltage_off->value());
}

void MainWindow::on_spin_smooth_valueChanged(int arg1)
{
    PL->SendInt16(10,((quint16)arg1 * 50));
}

void MainWindow::on_button_save_clicked()
{
    PL->SendInt8(11,1);
}

void MainWindow::on_check_input1_clicked()
{
    PL->SendInt8(1,(quint8)ui->check_input1->isChecked());
}

void MainWindow::on_check_input2_clicked()
{
    PL->SendInt8(2,(quint8)ui->check_input2->isChecked());
}

void MainWindow::on_check_input3_clicked()
{
    PL->SendInt8(3,(quint8)ui->check_input3->isChecked());
}

void MainWindow::on_check_input4_clicked()
{
    PL->SendInt8(4,(quint8)ui->check_input4->isChecked());
}

void MainWindow::on_check_input5_clicked()
{
    PL->SendInt8(5,(quint8)ui->check_input5->isChecked());
}

void MainWindow::on_button_info_clicked()
{
    About ab(this);
    ab.exec();
}

void MainWindow::on_spin_delay_valueChanged(int arg1)
{
   PL->SendInt16(16,(quint16)arg1);
   ui->output_delay_progress->setMaximum(arg1);
}
