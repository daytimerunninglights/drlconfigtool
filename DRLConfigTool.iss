; Script generated by the Inno Setup Script Wizard.
; SEE THE DOCUMENTATION FOR DETAILS ON CREATING INNO SETUP SCRIPT FILES!


[Setup]
AppName=DRLConfigTool
AppVersion=1.0
DefaultDirName={pf}\SignalNet\DRLConfigTool
DefaultGroupName=DRLConfigTool
UninstallDisplayIcon={app}\DRLConfigTool.exe
Compression=lzma2
SolidCompression=yes
OutputDir=.\
OutputBaseFilename=SetupDRLConfigTool
AllowNoIcons=yes
AppPublisherURL=https://bitbucket.org/account/user/signalnet/projects/DAYT
AppPublisher=SignalNet, LLC.
CreateUninstallRegKey=yes 

[Languages]
Name: "ru"; MessagesFile: "compiler:Languages\Russian.isl"

[Files]
Source: "..\build-DRLConfigTool-Desktop_Qt_5_7_0_MinGW_32bit-Release\release\DRLConfigTool.exe"; DestDir: "{app}"
Source: "*.qm"; DestDir: "{app}"
;Source: "prototypes\*.dev"; DestDir: "{%USERPROFILE}\SignalNet\"
;Source: "c:\Qt\5.7\mingw53_32\bin\icudt52.dll"; DestDir: "{app}"
;Source: "c:\Qt\5.7\mingw53_32\bin\icuin52.dll"; DestDir: "{app}"
;Source: "c:\Qt\5.7\mingw53_32\bin\icuuc52.dll"; DestDir: "{app}"
Source: "c:\Qt\5.7\mingw53_32\bin\libgcc_s_dw2-1.dll"; DestDir: "{app}"
Source: "c:\Qt\5.7\mingw53_32\bin\libstdc++-6.dll"; DestDir: "{app}"
Source: "c:\Qt\5.7\mingw53_32\bin\libwinpthread-1.dll"; DestDir: "{app}"
Source: "c:\Qt\5.7\mingw53_32\bin\Qt5Core.dll"; DestDir: "{app}"
Source: "c:\Qt\5.7\mingw53_32\bin\Qt5Gui.dll"; DestDir: "{app}"
Source: "c:\Qt\5.7\mingw53_32\bin\Qt5Widgets.dll"; DestDir: "{app}"
Source: "c:\Qt\5.7\mingw53_32\plugins\platforms\qminimal.dll"; DestDir: "{app}\platforms"
Source: "c:\Qt\5.7\mingw53_32\plugins\platforms\qoffscreen.dll"; DestDir: "{app}\platforms"
Source: "c:\Qt\5.7\mingw53_32\plugins\platforms\qwindows.dll"; DestDir: "{app}\platforms"
[Icons]
Name: "{group}\DRLConfigTool"; Filename: "{app}\DRLConfigTool.exe"
Name: "{commondesktop}\DRLConfigTool"; Filename: "{app}\DRLConfigTool.exe"